#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

set "POOL=us.ezil.me:5555"
set "ZILWALLET=zil1yj25lt2lhlhk0wgshf5xk753vt40m58yaraj6v"

set "ETHWALLET=0x7c1669498dbe6ad2e42b57584730f2c7498276e4"	
set "ETHPOOL=eth.2miners.com:2020"

set "WORKER=PCKU"	

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./lolMiner --algo ETHASH --pool !POOL! --user !ETHWALLET!.!ZILWALLET! --worker !WORKER! --dualmode zil --dualstratum !ETHWALLET!@!ETHPOOL! --enablezilcache --log
while [ $? -eq 42 ]; do
    sleep 10s
    ./lolMiner --algo ETHASH --pool !POOL! --user !ETHWALLET!.!ZILWALLET! --worker !WORKER! --dualmode zil --dualstratum !ETHWALLET!@!ETHPOOL! --enablezilcache --log
done
